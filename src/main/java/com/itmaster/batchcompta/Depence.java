package com.itmaster.batchcompta;

import lombok.ToString;

import java.util.Date;

@ToString
public class Depence {

    private Long id;
    private String libelle;
    private float prix;
    private String memo;
    private Date dateChargement;
    private String codeRetour;

    public Depence() {
    }

    public Depence(String libelle, float prix, String memo) {
        this.libelle = libelle;
        this.prix = prix;
        this.memo = memo;
    }

    public Depence(Long id, String libelle, float prix, String memo, Date dateChargement, String codeRetour) {

        this(libelle, prix, memo);
        this.id = id;
        this.dateChargement = dateChargement;
        this.codeRetour = codeRetour;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public Date getDateChargement() {
        return dateChargement;
    }

    public void setDateChargement(Date dateChargement) {
        this.dateChargement = dateChargement;
    }

    public String getCodeRetour() {
        return codeRetour;
    }

    public void setCodeRetour(String codeRetour) {
        this.codeRetour = codeRetour;
    }

    @Override
    public String toString() {
        return "Depence{"+libelle + ',' + prix + ','+ memo + ','+ dateChargement + ','+ codeRetour +'}';
    }
}
