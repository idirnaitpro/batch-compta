package com.itmaster.batchcompta;

        import org.slf4j.Logger;
        import org.slf4j.LoggerFactory;

        import org.springframework.batch.item.ItemProcessor;
        import org.springframework.util.StringUtils;

        import java.util.Date;

public class DepenceItemProcessor implements ItemProcessor<Depence, Depence> {

    private static final Logger log = LoggerFactory.getLogger(DepenceItemProcessor.class);

    @Override
    public Depence process(final Depence depence) throws Exception {

        depence.setDateChargement(new Date());

        if(StringUtils.isEmpty(depence.getLibelle()))
            depence.setCodeRetour(CodeRetourEnum.LIGNE_LIBELLE_NULL.getCode());


        if(depence.getCodeRetour() != null & StringUtils.isEmpty(depence.getPrix()))
            depence.setCodeRetour(CodeRetourEnum.LIGNE_PRIX_NULL.getCode());

        if(depence.getCodeRetour() == null)
            depence.setCodeRetour(CodeRetourEnum.LIGNE_OK.getCode());

        return depence;
    }

}
