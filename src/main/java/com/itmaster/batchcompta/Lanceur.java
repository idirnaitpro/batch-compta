package com.itmaster.batchcompta;


        import org.springframework.boot.SpringApplication;
        import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lanceur {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Lanceur.class, args);
    }
}
