package com.itmaster.batchcompta;


public enum CodeRetourEnum {

    LIGNE_OK ("0","OK"),
    LIGNE_LIBELLE_NULL ("LN","Ligne avec libbele null"),
    LIGNE_PRIX_NULL ("PN","Ligne avec prix null");

    private String code;
    private String description;

    CodeRetourEnum(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

}
